package com.freewayso.image.combiner.painter;

import com.freewayso.image.combiner.element.CombineElement;
import com.freewayso.image.combiner.element.RectangleElement;
import com.freewayso.image.combiner.enums.Direction;

import java.awt.*;

/**
 * @Author zhaoqing.chen
 * @Date 2020/8/21
 * @Description 矩形绘制器
 */
public class RectanglePainter implements IPainter {

    @Override
    public void draw(Graphics2D g, CombineElement element, int canvasWidth) {

        //强制转成子类
        RectangleElement rectangleElement = (RectangleElement) element;

        //设置颜色
        g.setColor(rectangleElement.getColor());

        //设置居中（优先）和绘制方向
        if (rectangleElement.isCenter()) {
            int centerX = (canvasWidth - rectangleElement.getWidth()) / 2;
            rectangleElement.setX(centerX);
        } else if (rectangleElement.getDirection() == Direction.RightLeft) {
            rectangleElement.setX(rectangleElement.getX() - rectangleElement.getWidth());
        } else if (rectangleElement.getDirection() == Direction.CenterLeftRight) {
            rectangleElement.setX(rectangleElement.getX() - rectangleElement.getWidth() / 2);
        }

        //设置渐变
        if (rectangleElement.getFromColor() != null) {
            float fromX = 0, fromY = 0, toX = 0, toY = 0;
            switch (rectangleElement.getGradientDirection()) {
                case TopBottom:
                    fromX = rectangleElement.getX() + rectangleElement.getWidth() / 2;
                    fromY = rectangleElement.getY() - rectangleElement.getFromExtend();
                    toX = fromX;
                    toY = rectangleElement.getY() + rectangleElement.getHeight() + rectangleElement.getToExtend();
                    break;
                case LeftRight:
                    fromX = rectangleElement.getX() - rectangleElement.getFromExtend();
                    fromY = rectangleElement.getY() + rectangleElement.getHeight() / 2;
                    toX = rectangleElement.getX() + rectangleElement.getWidth() + rectangleElement.getToExtend();
                    toY = fromY;
                    break;
                case LeftTopRightBottom:
                    fromX = rectangleElement.getX() - (float) Math.sqrt(rectangleElement.getFromExtend());
                    fromY = rectangleElement.getY() - (float) Math.sqrt(rectangleElement.getFromExtend());
                    toX = rectangleElement.getX() + rectangleElement.getWidth() + (float) Math.sqrt(rectangleElement.getToExtend());
                    toY = rectangleElement.getY() + rectangleElement.getHeight() + (float) Math.sqrt(rectangleElement.getToExtend());
                    break;
                case RightTopLeftBottom:
                    fromX = rectangleElement.getX() + rectangleElement.getWidth() + (float) Math.sqrt(rectangleElement.getFromExtend());
                    fromY = rectangleElement.getY() - (float) Math.sqrt(rectangleElement.getFromExtend());
                    toX = rectangleElement.getX() - (float) Math.sqrt(rectangleElement.getToExtend());
                    toY = rectangleElement.getY() + rectangleElement.getHeight() + (float) Math.sqrt(rectangleElement.getToExtend());
                    break;
            }
            g.setPaint(new GradientPaint(fromX, fromY, rectangleElement.getFromColor(), toX, toY, rectangleElement.getToColor()));
        } else {
            g.setPaint(null);
        }

        //设置透明度
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, rectangleElement.getAlpha()));

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //根据是否有边框和圆角，采用不同的绘制方法
        if (rectangleElement.getBorderSize() != null) {
            g.setStroke(new BasicStroke(rectangleElement.getBorderSize()));
            //圆角是0要用drawRect，否则border大的时候会看出顶角线条不闭合
            if (rectangleElement.getRoundCorner() != 0) {
                g.drawRoundRect(rectangleElement.getX(), rectangleElement.getY(), rectangleElement.getWidth(), rectangleElement.getHeight(), rectangleElement.getRoundCorner(), rectangleElement.getRoundCorner());
            } else {
                g.drawRect(rectangleElement.getX(), rectangleElement.getY(), rectangleElement.getWidth(), rectangleElement.getHeight());
            }
        } else {
            if (rectangleElement.getRoundCorner() != 0) {
                g.fillRoundRect(rectangleElement.getX(), rectangleElement.getY(), rectangleElement.getWidth(), rectangleElement.getHeight(), rectangleElement.getRoundCorner(), rectangleElement.getRoundCorner());
            } else {
                g.fillRect(rectangleElement.getX(), rectangleElement.getY(), rectangleElement.getWidth(), rectangleElement.getHeight());
            }
        }
    }

    @Override
    public void drawRepeat(Graphics2D g, CombineElement element, int canvasWidth, int canvasHeight) {
        //强制转成子类
        RectangleElement rectangleElement = (RectangleElement) element;

        int currentX = element.getX();
        int currentY = element.getY();

        //起始坐标归位
        while (currentX > 0) {
            currentX = currentX - rectangleElement.getRepeatPaddingHorizontal() - rectangleElement.getWidth();
        }
        while (currentY > 0) {
            currentY = currentY - rectangleElement.getRepeatPaddingVertical() - rectangleElement.getHeight();
        }

        int startY = currentY;

        //从左往右绘制
        while (currentX < canvasWidth) {
            int i = 0;
            rectangleElement.setX(currentX);
            currentX = currentX + rectangleElement.getRepeatPaddingHorizontal() + rectangleElement.getWidth();
            //从上往下绘制
            while (currentY < canvasHeight) {
                //偶数行错位效果
                if (i % 2 != 0) {
                    rectangleElement.setX(rectangleElement.getX() - rectangleElement.getRepeatRowOffset());
                }

                rectangleElement.setY(currentY);
                currentY = currentY + rectangleElement.getRepeatPaddingVertical() + rectangleElement.getHeight();
                draw(g, rectangleElement, canvasWidth);

                //还原偏移的x坐标
                if (i % 2 != 0) {
                    rectangleElement.setX(rectangleElement.getX() + rectangleElement.getRepeatRowOffset());
                }
                i++;
            }

            //重置y坐标
            currentY = startY;
        }
    }
}

